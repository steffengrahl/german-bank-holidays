# German Bank Holidays

Use this library to check if a given date is a public holiday in Germany in 
general or in one of its states.

## Requirements

- PHP 7.4 or 8.0
- composer

## Installation

You can install this library in your project using composer. For more 
information about composer visit https://getcomposer.org

`composer require steffengrahl/german-bank-holidays`

## License

Please see [LICENSE.md](LICENSE.md) for more information
